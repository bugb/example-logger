const { createLogger, format, transports, config } = require('winston');
const { combine, timestamp, json } = format;

const log = createLogger({
  levels: config.syslog.levels,
  defaultMeta: { component: 'example-service' },
  format: combine(
    timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    json()
  ),

  transports: [
    new transports.Console(),
    new transports.File({ filename: 'log.txt' })
  ]
});


module.exports = log;